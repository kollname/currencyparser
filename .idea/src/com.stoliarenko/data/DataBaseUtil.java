package com.stoliarenko.data;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.imageio.IIOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DataBaseUtil {
    public static final String DB_DRIVER_CLASS="driver.class.name";
    public static final String DB_USERNAME="db.username";
    public static final String DB_PASSWORD="db.password";
    public static final String DB_URL = "db.url";

    private static Properties properties = null;
    private static MysqlDataSource dataSource;

    static {
        try{
            properties = new Properties();
            properties.load(new FileInputStream("src/main/resources/database.properties"));

            dataSource = new MysqlDataSource();
            dataSource.setUrl(properties.getProperty(DB_URL));
            dataSource.setUser(properties.getProperty(DB_USERNAME));
            dataSource.setPassword(properties.getProperty(DB_PASSWORD));


        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static MysqlDataSource getDataSource() {
        return dataSource;
    }
}
