package com.stoliarenko;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static final String ANSI_GREEN = "\u001B[32m";

    public static void main(String[] args) {
        try {
            URL url = new URL("https://finance.rambler.ru/currencies/GBP/?updated");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", "Chrome");

            int responseCode = connection.getResponseCode();
            System.out.println("Response code: " + responseCode);
            if (responseCode != 200) {
                System.out.println("Error reading web page");
                return;
            }

            BufferedReader inputReader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuilder sb = new StringBuilder();
            StringBuilder extractedText = new StringBuilder();

            while ((line = inputReader.readLine()) != null) {
                sb.append(line);
                System.out.println(line);
            }
            String tdToFind = "(<td>)(.+?)(</td>)";
            Pattern tdTextPattern = Pattern.compile(tdToFind);
            Matcher tdTextMatcher = tdTextPattern.matcher(sb);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            LocalDateTime now = LocalDateTime.now();




            while (tdTextMatcher.find()){
                extractedText.append(tdTextMatcher.group(2));
            }
            String findNumberThree = "3";
            String date = "([\\d]{2}:[\\d]{2}&[\\D]{4}\\;)([\\d]{2}\\.[\\d]{2}\\.[\\d]{4})(&[\\D]{4}\\;&[\\D]{4}\\;)([1-9][\\d]{1}\\.[\\d]{4})";
            Pattern dateTextPattern = Pattern.compile(date);
            Matcher dateMatcher = dateTextPattern.matcher(extractedText);
            while (dateMatcher.find()){
                if(dateMatcher.group(2).equals(dtf.format(now))){
                    System.out.println(ANSI_GREEN + "1 Pound costs: " + dateMatcher.group(4) + " RUB");
                }
            }


            inputReader.close();
        } catch (MalformedURLException e) {
            System.out.println("Malfored URL" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IOExeption: " + e.getMessage());
        }
    }
}
